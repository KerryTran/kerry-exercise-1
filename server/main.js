import { Meteor } from 'meteor/meteor';
import {TasksCollection} from '/imports/api/tasks';

function insertLink({ _id, text }) {
  TasksCollection.insert({text ,createdAt: new Date()});
}

Meteor.startup(() => {

  // if (!Accounts.findUserByUsername('admin')) {
  //   Accounts.createUser({
  //     username: 'admin',
  //     password: 'admin'
  //   });
  // }


  // If the Links collection is empty, add some data.
  if (TasksCollection.find().count() === 0) {
    insertLink({
      text: 'First Task',
      // isChecked: false
    });

    insertLink({
      text: 'Second Task',
      // isChecked: true
    });

    insertLink({
      text: 'Third Task',
      // isChecked: false
    });

    insertLink({
      text: 'Fourth Task',
      // isChecked: true
    });
  }
});
