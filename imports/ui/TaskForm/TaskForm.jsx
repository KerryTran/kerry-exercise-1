import React, { useState } from "react";
import { TasksCollection } from "/imports/api/tasks";



const handleSubmit = (e) => {
  // TasksCollection.insert({
  //   text: text.trim(),
  //   createdAt: new Date(),
  // });

  const value = e.target.value
  Meteor.call("task.insert", value.trim())
}


export default TaskForm = ({user}) => {
 
  return (
    <form  className="task-form">
      <input type="text" name="text" placeholder="Type to add new task"  onBlur={e => handleSubmit(e)} />
    </form>
  );
};
