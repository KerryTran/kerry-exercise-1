import React, { useState } from "react";
import { Task } from "./Task/Task";
import { TasksCollection } from "/imports/api/tasks";
import { useTracker } from "meteor/react-meteor-data";
import TaskForm from "./TaskForm/TaskForm";
import { LoginForm } from "./LoginForm/LoginForm";
import _ from "lodash";

const toggleCheck = ({ _id, isChecked }) => {
  Meteor.call("tasks.setChecked", _id, !isChecked);
};

const deleteTask = ({ _id }) => Meteor.call("tasks.remove", _id);

const togglePrivate = ({ _id, isPrivate }) => {
  Meteor.call("tasks.setPrivate", _id, !isPrivate);
};

export const App = () => {
  const filter = {};
  const { tasks, incompleteTasksCount, user } = useTracker(() => {
    Meteor.subscribe("tasks");

    return {
      tasks: TasksCollection.find(filter, { sort: { createdAt: -1 } }).fetch(),
      incompleteTasksCount: TasksCollection.find({
        isChecked: { $ne: true },
      }).count(),
      user: Meteor.user(),
    };
  });

  const [hideCompleted, setHideCompleted] = useState(false);
  const [userTask, setUserTask] = useState(null);

  if (hideCompleted) {
    _.set(filter, "checked", false);
  }

  if (!user) {
    return (
      <div className="simple-todos-react">
        <LoginForm />
      </div>
    );
  }

  return (
    <div className="simple-todos-react">
      <div className="task-header-style">
        <div className="task-form-header">
          <h1>Todo List ({incompleteTasksCount})</h1>
          <div>
            <label>
              <input
                type="checkbox"
                readOnly
                checked={Boolean(hideCompleted)}
                onClick={() => setHideCompleted(!hideCompleted)}
              />
              Hide Completed Tasks
            </label>
          </div>
        </div>
        <div>
          <select name="" id="" onChange={e => setUserTask(e.target.value)}>
            <option>----</option>
            {tasks.map((value) => (
              <option key={value._id} value={value._id}>
                {value.text}
              </option>
            ))}
          </select>
        </div>

        <TaskForm user={user} />
      </div>

      <ul className="tasks">
        {tasks.map((data) => (
          <Task
            task={data}
            key={data._id}
            onCheckboxClick={toggleCheck}
            onDeleteClick={deleteTask}
            onTogglePrivateClick={togglePrivate}
            userTask={userTask}
          />
        ))}
      </ul>
    </div>
  );
};
