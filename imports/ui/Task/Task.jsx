import React from "react";
import classnames from "classnames";

export const Task = ({
  task,
  onCheckboxClick,
  onDeleteClick,
  onTogglePrivateClick,
  userTask,
}) => {
  const classes = classnames("task", {
    checked: Boolean(task.isChecked),
  });

  return (
    <li className={classes}>
      <input
        type="checkbox"
        onClick={() => onCheckboxClick(task)}
        checked={Boolean(task.isChecked)}
        readOnly={true}
      />

      {userTask !== null ? (
        userTask === task._id ? (
          <button
            onClick={() => onTogglePrivateClick(task)}
            className="task-item-text"
          >
            {task.isPrivate ? "Private" : "Public"}
          </button>
        ) : null
      ) : null}
      <span className="task-item-text">
        {task.text} {task.username && <i>({task.username})</i>}
      </span>
      <button
        onClick={() => onDeleteClick(task)}
        className="simple-todos-react-button"
      >
        &times;
      </button>
    </li>
  );
};
